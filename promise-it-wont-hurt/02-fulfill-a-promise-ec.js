// 02-fulfill-a-promise-ec.js
//https://github.com/getify/You-Dont-Know-JS/blob/master/async%20%26%20performance/ch3.md

var x;
var y = 2;

console.log(x + y); // NaN cause x is undefined
