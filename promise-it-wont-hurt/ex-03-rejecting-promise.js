"use strict";
/**
exercise 3 "Reject a promise"
https://github.com/stevekane/promise-it-wont-hurt

Note to self after passing this one - 
    1. put each line of instruction before the code it relates to.
    2. Compare my function to boilerplate

## Task

Create a promise that after a delay of 300ms, rejects with an Error object.
The Error object should be constructed with parameter 'REJECTED!', which is
the textual message of the error.

Create a function onReject to print error.message using console.log. Pass
this function as a rejection handler to the then method of your promise.
**/

//function assignment / statement
var promise = new Promise(function (resolve, reject) {
    
    // must reject with an Error object!
    setTimeout(
        function () {
            //note - rejected handler? what's this called?
            reject(
                new Error("REJECTED!")
            );
        },
        300
    );

});

//function declaration
function onReject(error) {
    console.log(error.message);
}

promise
    .then(
        null,
        onReject
    );

// promise
//     .then(
//         null,//nifty, just pass null since we're not defining resolved
//         function (rejected) {// I wrote this more complicated than necessary.
//             onReject(rejected);// second param is a callback - a function. no `()`
//         }
//     );



// quiz could be reworded to make it more clear. See
// https://github.com/stevekane/promise-it-wont-hurt/blob/master/exercises/reject_a_promise/solution/solution.js
// see screenshot promises-workshopper-pass-fail-error-feedback.png
// compare to prompt

// Javascript Global Object : Standard Built-in objects - Error
// the Error constructor creates an error object
// Syntax of Error constructor
// new Error([message[, fileName[, lineNumber]]]);
// message, fileName, lineNumber are optional. 
// fileName, lineNumber are non-standard
//
// Error.prototype.message is a standard property
// Error.prototype.name is a standard property
