"use strict";
/**
A promise is either
        * fulfilled
        * rejected
        * pending
    Pending is the state of the promise waiting to be
        either fulfilled or rejected
    (resolved/settled means the promise has
         either fulfilled or rejected)
**/
/**
    These instructions/examples make no sense to me,
    in fact I'm more confused than ever even though 
    I've implemented promises in the wild with Q. (trailerpark site) 
    So:
        https://developers.google.com/web/fundamentals/getting-started/primers/promises
**/

// Use the ES6 promise polyfill if you aren't using
//     a recent version of Node, or the browser()
//run `npm install es6-promise`

//require("es6-promise");

var promise = new Promise(function (fulfill, reject) {
    //executor - single argument the Promise constructor takes
    //simulate async with setTimeout.
    // in the wild - use XHR, HTML API (fetch?)
    setTimeout(function () {
            fulfill("FULFILLED!");
            reject(Error("Something broke."));
    }, 300);


});

promise.then(
    function (result) {
        console.log(result);
    },
    function (err) {
        console.log(err);
    }
);

///now we use resolve and reject
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise

// when it's done it's settled - either fulfilled or rejected
// only pending before it's settled

// Promise.prototype.

// function messageOnFulfillment() {
//     setTimeout(function () {
//         console.log("FULFILLED!");
//     }, 300);
// }

// promise
//     .then(
//         messageOnFulfillment(),
//         function (err) {
//            console.log(err);
//         }
//     );
