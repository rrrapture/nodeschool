"use strict";
/*
    Promises workstop exercise 01
    ECMAScript 2015 aka ES6
    promises extend the Promise/A+ spec

    A promise is an object that defines
        a method called `then`. The promise
        object represents a value that may
        be available some time in the future.
        A promise symplifies async logic in JS.
 */
/*
    remember a function declaration NO ; after
        function monkeys(number) {
            console.log(number + " of Monkeys.");
        }

    however a function statement needs a semi

refresh of David Crockford's JS code conventions -
    http://javascript.crockford.com/code.html

    If . period is the first character on a 
    line, the indentation is increased by 4 spaces.            
 */
/*
    Traditional style of asynchronous callbacks,
        with no error handling:
 */
// Parse.User.logIn("user", "pass", {
//     success: function (user) {
//         query.find({
//             success: function (results) {
//                 results[0].save(
//                     {
//                         key: value
//                     },
//                     {
//                         success: function (result) {
//                             // the object was saved
//                         }
//                     }
//                 );
//             }
//         });
//     }
// });

/* Promise workflow, with 'first-class' error handling */

// Parse.User.logIn("user", "pass")
//     .then(
//         function (user) {
//             return query.find();
//         }
//     )
//     .then(
//         function (results) {
//             return results[0].save({key: value});
//         }
//     )
//     .then(
//         function (result) {
//             // the object was saved
//         }
//     )
//     .catch(
//         function (err) {
//             // error happened somewhere
//         }
//     );

//Using setTimeout, print the string 'TIMED OUT!' after 300ms.
//classic callback
setTimeout(
    function () {
        console.log("TIMED OUT!");
    },
    300
);




















