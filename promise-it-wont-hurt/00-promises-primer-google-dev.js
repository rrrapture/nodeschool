"use strict";
// https://developers.google.com/web/fundamentals/getting-started/primers/promises

/** The promise constructor takes one argument--
    a function with two parameters, 
        1. resolve (this is different from
        the fulfilled term in the ndoeschool
        tutorial.
        2. reject
    Do something within the callback, could be async
        (could I get an
        example?) then call resolve if everything
        works, and otherwise call reject.
    It's customary to reject with an Error object
        (an Error object captures a stack trace,
        which is useful in debugging.)
        
    
**/

/**
quote: https://davidwalsh.name/promises
Promises in the Wild
The XMLHttpRequest API is async but does not use the Promises API.
There are a few native APIs that now use promises, however:

Battery API
fetch API (XHR's replacement)
ServiceWorker API (post coming soon!)
Promises will only become more prevalent so it's important 
that all front-end developers get used to them.  
It's also worth noting that Node.js is another platform for Promises 
(obviously, as Promise is a core language feature).

Testing promises is probably easier than you think 
because setTimeout can be used as your async "task"!
**/
var promiseTemplate = new Promise(function(resolve, reject) {
    setTimeout(function timeoutMessage() {
        console.log("Timeout!");
    }, 500);

    if (/*if what? how do i test if setTimeout executed?*/) {
        resolved("Stuff worked");
    } else {
        reject(Error("promise rejected"));
    }
}

var promiseTemplate = new Promise(function(resolve, reject) {
  // do a thing, possibly async, then…
    

  if (/* everything turned out fine */) {
    resolve("Stuff worked!");
  }
  else {
    reject(Error("It broke"));
  }
});

//this is how you call the promise:

promiseTemplate.then(
    function (result) {
        console.log(result);// Stuff worked
    },
    function (err) {
        console.log(err);//Error: It broke
    }
);





