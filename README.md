#A repository of my nodeschool exercises

JavaScript, ES6, Node

From the [nodeschool.io workshoppers list](http://nodeschool.io/#workshoppers), plus 'extra-credit' examples of my own.

##Remote origin
[https://github.com/Rrrapture/nodeschool](https://github.com/Rrrapture/nodeschool)
