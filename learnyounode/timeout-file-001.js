// Animate the unit
   // Animate text and cta
   setTimeout(function(){
     container.classList.remove('gone');
     // Start the TV static
     setTimeout(function(){
         window.onresize();
        window.requestAnimationFrame(render);
     }, 20);
   }, 200);
   
   setTimeout(function(){
       body.classList.add('frame2');
       setTimeout(function(){
           var h1 = document.getElementsByClassName('heading1')[0];
           h1.classList.add('line2');
       }, 300);
       setTimeout(function(){
           var h1 = document.getElementsByClassName('heading1')[0];
           h1.classList.remove('line2');
           h1.classList.add('line3');
       }, 600);
   }, 400);
 

   setTimeout(function(){
       body.classList.remove('frame2');
       body.classList.add('frame3');
       setTimeout(function(){
           var h1 = document.getElementsByClassName('heading2')[0];
           h1.classList.add('line2');
       }, 50);
       setTimeout(function(){
           var h1 = document.getElementsByClassName('heading2')[0];
           h1.classList.remove('line2');
           h1.classList.add('line3');
       }, 1500);
   }, 2800);

   setTimeout(function(){
       body.classList.remove('frame3');
       body.classList.add('frame4');
   }, 6500);
   
   setTimeout(function(){
       body.classList.remove('frame4');
       body.classList.add('frame5');
   }, 8500);

   setTimeout(function(){
       goTruthFlickerInterval(); //1700 milliseconds
       body.classList.remove('frame5');
       body.classList.add('frame6');        
   }, 11000);
   

   if (version == 'enlist') {
        setTimeout(function(){
            body.classList.remove('frame6');
            body.classList.add('frame7');
            setTimeout(function(){
                var h1 = document.getElementsByClassName('heading3')[0];
                h1.classList.add('line2');
            }, 100);
            setTimeout(function(){
                var h1 = document.getElementsByClassName('heading3')[0];
                h1.classList.remove('line2');
                h1.classList.add('line3');
            }, 850);
        }, 12250);

        setTimeout(function(){
            body.classList.remove('frame7');
            body.classList.add('frame8');
            setTimeout(function(){
                var h1 = document.getElementsByClassName('heading4')[0];
                h1.classList.add('line2');
            }, 100);
            setTimeout(function(){
                var h1 = document.getElementsByClassName('heading4')[0];
                h1.classList.remove('line2');
                h1.classList.add('line3');
            }, 850);
        }, 14000);

   } else {
       // WATCH VERSION
        setTimeout(function(){
            body.classList.remove('frame6');
            body.classList.add('frame7');
            setTimeout(function(){
                var h1 = document.getElementsByClassName('heading3')[0];
                h1.classList.add('line2');
            }, 100);
            setTimeout(function(){
                var h1 = document.getElementsByClassName('heading3')[0];
                h1.classList.remove('line2');
                h1.classList.add('line3');
            }, 850);
        }, 12250);
   }

   var ctaTime = version == 'enlist' ? 14350 : 14700;
   setTimeout(function(){
       body.classList.add('cta');
   }, ctaTime);
   setTimeout(function(){
       bannerDone = true;
   }, 15000);