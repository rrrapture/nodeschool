// extra-credit-find-replace-ex09-juggling-async.js
// stdout one line (in order) for all data (as string)
// for each of three files passed as CL arguments.

var
    concat,
    http,
    urls,
    dataChunk,
    counterLimit,
    files,
    fileAsString,
    path,
    filepath,
    fs;

concat = require('concat-stream');
http = require('http');
//urls = [process.argv[2], process.argv[3], process.argv[4]];
files = [process.argv[2]];
fs = require('fs');
path = require("path");
dataChunk = [];
counterLimit = 0;

// test file
// timeout-file-001.js
files.forEach(nameTimeouts);

function nameTimeouts(file) {
    filepath = path.join(__dirname, file);
    fs.readFile(filepath, function doneReading(err, fileContents) {
        fileAsString = fileContents.toString();
        saveStringToNewFile();
    });
}

function saveStringToNewFile() {
    fs.writeFile("timeout-file-002-cleaned.js", fileAsString, function logItYeah(err) {
            console.log("Did that work?");
    });
}

// test urls:
// http://nodeschool.io/ http://bltcommunications.com/ http://rrrapture.com/
// have to count callbacks
// urls.forEach(pipeData);

// function pipeData(url) {

//     http.get(url, function getRequest(res) {
//             var result,
//                 urlsIndex;

//             urlsIndex = urls.indexOf(url);

//             res
//                 .pipe(concat(function (data) {
//                     result = data.toString();
//                     counterLimit ++;
//                     dataChunk[urlsIndex] = result;

//                     if (counterLimit === urls.length) {
//                         for (var i = 0; i < dataChunk.length; i++) {
//                             console.log(dataChunk[i]);
//                         }
//                     }
//                 }));

//         })
//         .on('error', function (err) {
//             console.log('Got error ' + err);
//         });
// }
