//.map() vs .forEach()
//Array.prototype.map()
var myArray = [1, 6, 10, 25];

var mappedValue = myArray.map(function (item) {
    // return console.log("item is " + item);
    // console.log(item);
    // return item + 1;
    return item;
});

console.log(mappedValue);//[1, 6, 10, 25]

// .map() returns a new array and is chainable

// console.log(typeof mappedValue);//new array
// console.log(typeof myArray);//unchanged


//Array.prototype.map()
var methodForEach = myArray.forEach(function (item) {
    // return item;//console.log returns undefined below
    console.log(item);
});
//arr.forEach(function callback(currentValue, index, array) {
//your iterator
//}[, thisArg]);

//https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/forEach

// forEach executes the function once for every item in the array,
// and UNLIKE .map() or .reduce() 
// it always returns undefined, and is not chainable
 

console.log(methodForEach);

// not to be confused with jQuery.each(array, callback), 
// another iterator fn
// The $.each() function can be used to iterate over any collection, 
// whether it is an object or an array.


// hmm check this out:
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/
// cont - Reference/Global_Objects/Array/map
// Using map to reformat objects in an array





